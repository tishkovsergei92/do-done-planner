import { Component, OnInit } from '@angular/core';
import { ToDoService } from "../../services/to-do.service"

import { ToDo } from '../../models/ToDo'
import { Succeed } from "../../models/Succeed"

@Component({
    selector: 'app-to-do-list',
    templateUrl: './to-do-list.component.html',
    styleUrls: ['./to-do-list.component.scss']
})
export class ToDoListComponent implements OnInit {
    todos: ToDo[]

    constructor(private service: ToDoService) { }

    ngOnInit() {
        this.service.getAll().subscribe((data) => {
            this.todos = data;
        })
    }

    public edit(todo: ToDo): void {
        this.service.update(todo).subscribe(data => console.log(data))
    }

    public delete(todo: ToDo): void {
        this.service.delete(todo.id).subscribe(result => {
            if (result && result.succeed) {
                this.todos = this.todos.filter(item => item.id != todo.id );
            }
        })
    }
}
