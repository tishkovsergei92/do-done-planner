import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from "rxjs"

import { ToDo } from "../models/ToDo"
import { Succeed } from "../models/Succeed"

import { host } from '../../environments/environment'

@Injectable({
  providedIn: 'root'
})
export class ToDoService {
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(
    private httpClient: HttpClient
  ) { }

  public create(item: ToDo): Observable<boolean> {
    return null;
  }

  public getAll(): Observable<ToDo[]> {
    return this.httpClient.get<ToDo[]>(`${host}/todo`);
  }

  public getOne(id: string | number): Observable<ToDo> {
    return this.httpClient.get<ToDo>(`${host}/todo/${id}`);
  }

  public update(todo: ToDo): Observable<boolean> {
    return this.httpClient.put<boolean>(`${host}/todo/`, todo, this.httpOptions);
  }

  public delete(id: string | number): Observable<Succeed> {
    return this.httpClient.delete<Succeed>(`${host}/todo/${id}`, this.httpOptions);
  }
}
