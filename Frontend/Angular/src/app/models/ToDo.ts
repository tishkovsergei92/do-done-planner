export class ToDo {
    id: number;
    name: string;
    description: string;
    startTime: Date;
    endTime: Date;
    isDone: boolean
}
