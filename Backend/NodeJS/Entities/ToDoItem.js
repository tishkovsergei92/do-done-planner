function ToDoItem(id, name, description, startTime, endTime, isDone) {
    this.id = id;
    this.name = name;
    this.description = description;
    this.startTime = startTime;
    this.endTime = endTime;
    this.isDone = isDone;
}

module.exports = ToDoItem;