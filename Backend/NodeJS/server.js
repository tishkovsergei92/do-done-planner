/* 

D:\Gitlab\tishkovsergei92@gmail.com\do-done-planner\Scripts\RunNodeJsServer.bat

*/
const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser")

const toDoController = require("./Controllers/ToDoController");

const app = express();

app.use(cors());
app.use(bodyParser.json());
app.disable('etag');

app.use("/todo", toDoController);

app.listen(3000, () => console.log("Server started listening on port 3000"));


