const express = require("express");

const ToDoItem = require("../Entities/ToDoItem.js");

//TODO remove the stub and add normal DB instead
let toDos = [
    new ToDoItem(0, "Name1", "Descr1", new Date("2019-12-11T03:24:00"), new Date("2019-12-11T03:25:00"), false),
    new ToDoItem(1, "Name2", "Descr2", new Date("2019-12-11T03:28:00"), new Date("2019-12-11T03:28:00"), true),
]

const toDoController = express.Router();

const logRequest = (request) => {
    console.log('');
    console.log(`Request: ${request.method}; URL: ${request.protocol + '://' + request.get('host') + request.originalUrl}`);
}

const logResponse = (response) => {
    response.on("finish", () => {
        console.log(`response: ${response.statusCode}`);
        // console.log(`${response.body}`);
    })
}

const funcWithLogging = (func) => {
    return function (request, response) {
        logRequest(request);
        func(request, response);
        logResponse(response);
    }
}

//CREATE
const postCallback = funcWithLogging((request, response) => {
    let body = request.body;

    if (!body.name) {
        response.sendStatus(400);
    } else {
        let newItem = new ToDoItem(toDos.length, body.name, body.descr, body.startDate, body.endDate, body.isDone);
        toDos.push(newItem);

        response.sendStatus(200);
    }
})

// READ
const getAllCallback = funcWithLogging((request, response) => {
    response.json(toDos);
})
const getOneCallback = funcWithLogging((request, response) => {
    response.json(toDos.find(item => item.id === request.params["id"]));
})

//UPDATE
const putCallback = funcWithLogging((request, response) => {
    response.send(true);
})


//DELETE
const deleteCallback = funcWithLogging((request, response) => {
    let initialLength = toDos.length;
    toDos = toDos.filter(item => item.id != request.params["id"] );

    let succeed = { succeed: initialLength !== toDos.length ? true : false }
    
    response.status(succeed.succeed ? 200 : 404).json(succeed);
})

toDoController.get("/:id", getOneCallback)
toDoController.get("/", getAllCallback)
toDoController.put("/", putCallback)
toDoController.delete("/:id", deleteCallback)

module.exports = toDoController;